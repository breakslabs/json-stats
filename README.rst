=============
JSON Analyzer
=============

Simple shell utility to analyze token and type frequency in JSON
data. Examines raw JSON data without invoking **json.loads()** or
otherwise converting the content into python values, strictly through
syntactical and reference analysis. This should prove (relatively) safe even
on non-trusted data.

Can provide output as plain text, or a JSON document for further processing.

What's analysed:
----------------

+ strings
+ ints
+ floats
+ brackets ([])
+ braces ({})
+ string keys and string data
+ booleans (true, false)
+ null values

What's not presently analyzed
-----------------------------
 
- value frequencies aside from strings
- key frequencies aside from strings
- list/object/scalar frequencies
- root datatypes that aren't either an object or a list
- probably some other things I missed

Requirements
------------

Python3.7 or later and the stdlib. That's about it.

  
Installing
----------

Full Install
~~~~~~~~~~~~
It is recommended to set up a virtual environment instead of trashing your
public environment. On Linux/Unix/POSIX-like systems::
  python -mvenv json_stats
  cd json_stats
  . bin/activate

Or whatever the procedure is for your given OS.

Clone the repository::
  git clone https://gitlab.com/breakslabs/json-stats.git

  cd json-stats

Install the package and script::
  python ./setup.py install

Done.

Quick Script Install
~~~~~~~~~~~~~~~~~~~~

Clone the repository somewhere as above, and copy
'json-stats/src/json_stats/json_stats.py' somewhere useful. Or just
copy-and-paste the contents from GitLab into a file. Whatever works. The
script is self-contained and does not depend on the package in any way.
  
Usage
-----

Command line invocation::
  json_stats [-j] [input file[input file]...]

If the '-j' option is given, output will be a JSON document. Otherwise output
will be plain text. Input files may be given explicitly on the command line,
or a single document may be read from **stdin**. If any of the command-line
provided input files is a directory, all files in that directory whose names
end in '.json' will be processed.

Presently, all output is to stdout.

Why?
----

Why not?

Honestly, I wanted to see if there was a reliable heuristic for identifying
JSON data without reverting to "just attempt to parse it and bail out if it
breaks." I haven't done that yet. But I thought maybe other people could find
uses for it.


Future
------

Cover the stuff in :ref:`What's not presently analyzed`. Maybe check other
non-JSON things like prevalence of camelCasing and other common JSON tropes.

License
-------
See :doc:`LICENSE.txt`

