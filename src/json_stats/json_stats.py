#!/usr/bin/env python
"""
Examine JSON data without attempting to convert it to python objects

Counts certain JSON features like strings (keys or data), ints, floats, bools,
nulls, and control tokens like brackets, braces, colons, commas, &c. Then
calculates percentages of each in relation to the document size in bytes (both
with and without string data). Returns the data either as formatted plain text
or as a JSON document suitable for further processing (graphs, &c.).

If no filepath is given, reads from stdin. If any filepath given is a
directory, it will process all files found in that directory that end in
'.json'. All output is written to stdout.
"""
import argparse
from collections import namedtuple
from contextlib import contextmanager
import dataclasses
from decimal import ROUND_HALF_UP, Decimal as D
import enum
import itertools
import json
import pathlib
import re
import string
import sys
from typing import Any, Dict, Union, Optional, List


LCW = 22 # Label column width

Braces = namedtuple('Braces', ['left_count', 'right_count'])
Token = namedtuple('Token', ['count', 'size'])
StringInfo = namedtuple('StringInfo', ['string_count', 'strings_key',
                                       'strings_data', 'stringless'])


class PState(str, enum.Enum):
    START = 'start'
    OBJECT = 'object'
    LIST = 'list'
    KEY = 'key'
    
class TType(str, enum.Enum):
    OBJECT_START = '{'
    OBJECT_END = '}'
    LIST_START = '['
    LIST_END = ']'
    STRING_KEY = 'string_key'
    STRING_DATA = 'string_data'
    NUMBER = 'number'
    BOOL = 'bool'
    NULL = 'null'
    PUNCT = 'punctuation'
    WSPACE = 'whitespace'
    

Token = namedtuple('Token', ['tok_type', 'content'])
    
class InvalidJSON(Exception):
    pass


class SimpleView:
    def __init__(self, val, offset):
        self.val = val
        self.offset = offset

    def __getitem__(self, idx):
        if isinstance(idx, slice):
            start = idx.start
            stop = idx.stop
            start = start+self.offset if start is not None else self.offset
            stop = stop+self.offset if stop is not None else self.offset
            return self.val[slice(start, stop, idx.step)]
        return self.val[idx+self.offset]

    def __len__(self):
        return len(self.val) - self.offset
    
class JSONParser:
    discard_whitespace = True
    
    def __call__(self, jdata):
        self._state_stack = [PState.START]
        self.keystate = False
        self.total_whitespace = 0
        return self._parse(jdata)

    @property
    def state(self):
        return self._state_stack[-1]

    @state.setter
    def state(self, v):
        self._state_stack.append(v)

    def state_exit(self, expected):
        return self._state_stack.pop() == expected
    
    def _parse(self, jdata):
        cpos = 0
        line = 1
        lpos = 1
        tokens = list()
        while cpos < len(jdata):
            try:
                read, token = self._parse_next_token(SimpleView(jdata,cpos))
            except InvalidJSON as e:
                e.args = (f'line {line} pos {lpos} (char {cpos}) - {e}',)
                raise
            cpos += read
            lpos += read
            tokens.append(token)
            wspace = list()
            while cpos < len(jdata) and jdata[cpos].isspace():
                self.total_whitespace += 1
                wspace.append(jdata[cpos])
                if jdata[cpos] in '\n\r':
                    line += 1
                    lpos = 0
                else:
                    lpos += 1
                cpos += 1
            if wspace and not self.discard_whitespace:
                tokens.append(Token(TType.WSPACE, ''.join(wspace)))
        return tokens

    def _parse_next_token(self, data):
        c = data[0]
        if c == '"':
            read = self._parse_string(data)
            strv = data[:read]
            if self.state == PState.OBJECT and self.keystate:
                return (read, Token(TType.STRING_KEY, strv))
            else:
                return (read, Token(TType.STRING_DATA, strv))
        elif c in string.digits or c=='-':
              read = self._parse_numeral(data)
              return (read, Token(TType.NUMBER, data[:read]))
        elif c == '{':
            self.keystate = True
            self.state = PState.OBJECT
            return (1, Token(TType.OBJECT_START, c))
        elif c == '}':
            if not self.state_exit(PState.OBJECT):
                raise InvalidJSON(f'mismatched "}}"')
            return (1, Token(TType.OBJECT_END, c))
        elif c == '[':
            self.state = PState.LIST
            return (1, Token(TType.LIST_START, c))
        elif c == ']':
            if not self.state_exit(PState.LIST):
                raise InvalidJSON(f'mismatched "]"')
            return (1, Token(TType.LIST_END, c))
        elif c == ',':
            if self.state == PState.OBJECT:
                self.keystate = not self.keystate
            return (1, Token(TType.PUNCT, c))
        elif c==':':
            if self.state != PState.OBJECT:
                raise InvalidJSON('unexpected ":"')
            self.keystate = not self.keystate
            return (1, Token(TType.PUNCT, c))
        elif data[:4] == 'null':
            return (4, Token(TType.NULL, data[:4]))
        elif data[:4] == 'true':
            return (4, Token(TType.BOOL, data[:4]))
        elif data[:5] == 'false':
            return (5, Token(TType.BOOL, data[:5]))
        else:
            raise InvalidJSON(f'unexpected character "{c}"')
        
    def _parse_string(self, data):
        assert data[0] == '"'
        escaped = False
        pos = 1
        parsing = True
        while parsing and pos < len(data):
            c = data[pos]
            if c == '"' and not escaped:
                parsing = False
            elif c=='\\':
                escaped = not escaped
            else:
                escaped = False
            pos += 1
        if parsing:
            raise InvalidJSON('unexpected end of input')
        return pos

    def _parse_numeral(self, data):
        # NB not true float parser - just looks for "number-y things"
        alldigits = string.digits+'-.eE'
        c = data[0]
        pos = 0
        parsing = True
        while parsing and pos<len(data):
            c = data[pos]
            if c in alldigits:
                pos += 1
            elif c not in ',]}:' and not c.isspace():
                raise InvalidJSON(f'invalid number {c}')
            else:
                parsing = False
        if parsing:
            raise InvalidJSON('unexpected end of input')
        return pos
            


class LVEncoder(json.JSONEncoder):
    indent = 2
    def default(self, obj):
        if isinstance(obj, LabeledPercent):
            v = { 'label': obj.label, 'count': obj.count,
                  'size': obj.size,
                  'percent_of_stringless': obj.percent }
            if obj.doc_size is not None:
                v['percent_of_total'] = obj.percent_tot
            return v
        elif isinstance(obj, LabeledValue):
            return vars(obj)
        elif isinstance(obj, D):
            return float(obj)
        else:
            super().default(obj)

@dataclasses.dataclass
class LabeledValue:
    label: str
    value: Any

    def __str__(self):
        return f'{self.label:<{LCW}}{self.value}'

@dataclasses.dataclass
class LabeledPercent:
    label: str
    count: int
    size: int
    doc_stringless_size: int
    doc_size: Optional[int] = None

    @property
    def percent(self):
        if self.doc_stringless_size is not None:
            return D(self.size)/D(self.doc_stringless_size)
        return None
    
    @property
    def percent_tot(self):
        if self.doc_size is not None:
            return D(self.size)/D(self.doc_size)
        else:
            return None

    def __str__(self):
        if self.doc_stringless_size is not None:
            pct1 = (self.percent*100).quantize(D('0.0001'), ROUND_HALF_UP)
            pct_str1 = f'{pct1:3.4f}%'
            of1 = f'({self.size} of {self.doc_stringless_size})'
        else:
            pct_str1 = ''
            of1 = ''
        if self.doc_size is not None:
            pct2 = (self.percent_tot*100).quantize(D('0.0001'), ROUND_HALF_UP)
            pct_str2 = f'{pct2:3.4f}%'
            of2 = f'({self.size} of {self.doc_size})'
            str2 = f'  [{pct_str2:>9s} {of2} ]'
        else:
            str2=''
        return f'{self.label:<{LCW}}{pct_str1:>9s} {of1:<18s}{str2}'

class FTC:
    """FilterTypeContent - filter a list of Tokens by type and content"""
    ANY = type('ANY', (), {})
    IN = type('IN', (str,), {})
    NOT_IN = type('NOT_IN', (str,), {})
    
    def __init__(self, ttype: TType, char: str):
        self.c = char
        self.ttype = ttype
        
    def __call__(self, data: List[Token]) -> List[Token]:
        if self.c == self.ANY or isinstance(self.c, self.ANY):
            return [ t for t in data if t.tok_type == self.ttype ]
        elif isinstance(self.c, self.IN):
            return [ t for t in data
                    if t.tok_type == self.ttype and self.c in t.content ]
        elif isinstance(self.c, self.NOT_IN):
            return [ t for t in data
                    if t.tok_type == self.ttype and self.c not in t.content ]
        else:
            return [ t for t in data
                    if t.tok_type == self.ttype and t.content == self.c ]
    
def calc_stats(data: List[Token]) -> Dict[str,
                                          Union[LabeledPercent, LabeledValue]]:
    """Calculate percentages of JSON tokens in a given document
    
    :param data: result dictionary from :func:`examine_json`
    :return: dict of calculated values
    """
    VALLABS = {
        'colons': ('colons_percent', '% ":"', FTC(TType.PUNCT, ':')),
        'commas': ('commas_percent', '% ","', FTC(TType.PUNCT, ',')),
        'whitespace': ('whitespace_percent', '% whitespace',
                       FTC(TType.WSPACE, FTC.ANY)),
        'floats': ('floats_percent', '% floats',
                   FTC(TType.NUMBER, FTC.IN('.'))),
        'ints': ('ints_percent', '% ints', FTC(TType.NUMBER, FTC.NOT_IN('.'))),
        'bools': ('bools_percent', '% bools', FTC(TType.BOOL, FTC.ANY)),
        'nulls': ('nulls_percent', '% nulls', FTC(TType.NULL, FTC.ANY))
    }
    res = dict()
    size = sum([ len(t.content) for t in data ])
    s_keys = [ t for t in data if t.tok_type == TType.STRING_KEY ]
    s_data = [ t for t in data if t.tok_type == TType.STRING_DATA ]
    strings_size = sum([ len(t.content) for t in itertools.chain(s_keys, s_data) ])
    size_s = size - strings_size
    res['data_size'] = LabeledValue('Total bytes', size)
    res['stringless_size'] = LabeledValue('Len w/o strings', size_s)
    res['string_key_count'] = LabeledValue('# of string keys',
                                           len(s_keys))
    res['string_data_count'] = LabeledValue('# of data strings',
                                            len(s_data))
    res['string_percent'] = LabeledPercent( 'String % of total',
                                            len(s_keys) + len(s_data),
                                            strings_size, None, size)
    bcount = D(len([t for t in data
                    if t.tok_type in (TType.OBJECT_START, TType.OBJECT_END) ]))
    res['braces_percent'] = LabeledPercent('% "{" or "}"', bcount, bcount,
                                           size_s, size)
    #bsum = D(data['braces']['[]'].left_count
    #         + data['braces']['[]'].right_count)
    #bcount = (data['braces']['[]'].left_count,
    #          data['braces']['[]'].right_count)
    bcount = D(len([t for t in data
                    if t.tok_type in (TType.LIST_START, TType.LIST_END) ]))
    res['brackets_percent'] = LabeledPercent('% "[" or "]"', bcount, bcount,
                                             size_s, size)
    for k, v in VALLABS.items():
        l = v[2](data)
        tsize = sum([ len(t.content) for t in l ])
        res[v[0]] = LabeledPercent(v[1], len(l), tsize,
                                   size_s, size)
    return res

def parse_cmdline() -> argparse.Namespace:
    """Name's on the tin"""
    p = argparse.ArgumentParser(description=globals()['__doc__'])
    p.add_argument('-j', '--json', action='store_true', default=False,
                   help='return results as JSON instead of plain text')
    p.add_argument('datapath', type=pathlib.Path, default=None, nargs='*',
                   help='path to directory containing JSON data files')

    return p.parse_args()

def format_data(data: Dict[str, Union[LabeledPercent, LabeledValue]],
                name: str,
                asjson: bool) -> Union[Dict, list]:
    """Generate either a dict of values or a list of text reprs of said values

    :param data: data as provided by :func:`calc_stats`
    :param name: the name of this data group (e.g. the filename the data came
        from). This is used as either the text section title, or the primary
        dict key.
    :param asjson: determines whether we're producing the list of text
        (False), or the dict (True)
    :return: either a dict of values w/ labels, or a list of labeled results
        as text
    """
    tpct_total = D(0)
    pct_total = D(0)
    parser = JSONParser()
    parser.discard_whitespace = False
    res = parser(data)
    calced = calc_stats(res)
    
    if not asjson:
        result = [f'{name}']
        result.append('='*len(name))
    else:
        result = dict()
    for k, v in calced.items():
        if not asjson:
            result.append(str(v))
        else:
            result[k] = v
        if isinstance(v, LabeledPercent):
            if v.doc_size:
                pct_total += v.percent_tot
            if v.doc_stringless_size:
                tpct_total += v.percent
    if not asjson:
        tok_tot_pct = f'{tpct_total.quantize(D("0.0001"))*100:3.4f}%'
        tot_pct = f'{pct_total.quantize(D("0.0001"))*100:3.4f}%'
        result.append(f'{"Totals:":<{LCW}}{tok_tot_pct:<30}[{tot_pct}]')

        return result
    else:
        result['total_token_percent'] = tpct.quantize(D('0.0001'))
        return  { name: result }
        
        

def main():
    """Command-line entry point"""
    args = parse_cmdline()
    args.out = sys.stdout # FIX THIS
    output = list()
    if not args.datapath:
        jdata = sys.stdin.read()
        output.append(format_data(jdata, '<stdin>',  args.json))
    else:
        for file in args.datapath:
            file = file.expanduser()
            if file.is_dir():
                for d in file.iterdir():
                    if not d.name.endswith('.json'):
                        continue
                    jdata = d.read_text()
                    output.append(format_data(jdata, d.name, args.json))
            else:
                jdata = file.read_text()
                output.append(format_data(jdata, file.name, args.json))
    if args.json:
        odict = { k: v for i in output for k, v in i.items() }
        print(json.dumps(odict, cls=LVEncoder, indent=2))
    else:
        for l in output:
            print('\n'.join(l))
        
if __name__ == '__main__':
    main()
